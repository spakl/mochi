package server

import (
	"mochi/utils"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/sirupsen/logrus"
	oteltrace "go.opentelemetry.io/otel/trace"

)

type Server struct {
	Port string 
	Router *chi.Mux
	Logger *logrus.Logger
	Tracer *oteltrace.Tracer
	traceEnabled bool

}



func CustomServer(config utils.Config, router *chi.Mux, Logger *logrus.Logger) *Server {
	server := &Server{
		Port: config.Server.Port,
		Router: router,
		Logger: Logger,
		traceEnabled: config.Tracing.Enabled,
	}
	if config.Server.StartMessage {
		printIntro(config.Server.Port)
	}
	return server
}

func NewServer() *Server {

	config, err :=  utils.LoadConfig(".")
	if err != nil {
		logrus.Fatalf("Error loading config: %v", err)
	}

	Router := chi.NewRouter()
	Logger := logrus.New()
	Tracer := utils.Tracer
	server := &Server{
		Port: config.Server.Port,
		Router: Router,
		Logger: Logger,
		Tracer: &Tracer,
		traceEnabled: config.Tracing.Enabled,
	}
	if config.Server.StartMessage {
		printIntro(config.Server.Port)
	}
	return server
}


func (s *Server) InitCoreMiddleWare(){
	if s.traceEnabled {
		s.Router.Use(
			utils.OtelMiddleware,
			utils.TraceLoggerMiddleware,
		)
	} else {
		s.Router.Use(utils.LoggerMiddleware)
	}
}

func (s *Server) Start() error {

	s.Router.Get("/", helloWorld)
	// Apply auth middleware to only `GET /users/{id}`
	s.Router.Group(func(r chi.Router) {
		r.Get("/hello/{id}", helloWorld)
	})

	return http.ListenAndServe(":"+s.Port, s.Router)
}
