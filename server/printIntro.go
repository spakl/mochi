package server

import (
	"fmt"
	"os"
	"runtime"
)

func printIntro(port string) {
	mochiArt := `
-----------------------------------------------------------------------
    _           _                            _                _          
   (_) _     _ (_)                          (_)              (_)         
   (_)(_)   (_)(_)    _  _  _       _  _  _ (_) _  _  _    _  _          
   (_) (_)_(_) (_) _ (_)(_)(_) _  _(_)(_)(_)(_)(_)(_)(_)_ (_)(_)         
   (_)   (_)   (_)(_)         (_)(_)        (_)        (_)   (_)         
   (_)         (_)(_)         (_)(_)        (_)        (_)   (_)         
   (_)         (_)(_) _  _  _ (_)(_)_  _  _ (_)        (_) _ (_) _       
   (_)         (_)   (_)(_)(_)     (_)(_)(_)(_)        (_)(_)(_)(_)      

-----------------------------------------------------------------------
 `
 fmt.Println(mochiArt)
 fmt.Printf("Port: %s\n", port)
 fmt.Printf("Operating System: %s\n", runtime.GOOS)
 fmt.Printf("CPU Architecture: %s\n", runtime.GOARCH)
 fmt.Printf("Number of CPUs: %d\n", runtime.NumCPU())
 
 hostname, err := os.Hostname()
 if err == nil {
	 fmt.Printf("Hostname: %s\n", hostname)
	 } else {
		 fmt.Println("Could not get hostname")
		}
		
		fmt.Println("-----------------------------")
		fmt.Println("Mochi Server is Starting!")
		fmt.Println("-----------------------------")
}