package server

import (
	"mochi/types"
	"mochi/utils"
	"net/http"
	"github.com/go-chi/chi/v5"
)

func helloWorld(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	id := chi.URLParam(r, "id")
	_, span := utils.CreateSpanWithAttr(ctx, "helloWorld", "id", id)
	defer span.End()
	logger, ok := ctx.Value(utils.LoggerKey).(utils.ILogger)
	if !ok {
		errMsg := "Could not retrieve logger from context"
		logger.Error(errMsg)
		response := types.ResponseBody{
			Message: "An error has occured!",
			Path:    r.URL.String(),
			Data:    nil,
			Error:   errMsg,
		}
		utils.MochiJSON(w, response, http.StatusInternalServerError)
		return
	}

	logger.Debug("Handling Hello World")

	status := http.StatusOK
	response := types.ResponseBody{
		Message: "Hello, world!",
		Path:    r.URL.String(),
		Data:    nil,
	}
	utils.MochiJSON(w, response, status)
}
