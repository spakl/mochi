package main

import (
	"flag"
	"mochi/server"
	"mochi/utils"

	"github.com/go-chi/chi/v5"
	"github.com/sirupsen/logrus"
)




func main() {
	var portFlag string
	var configPath string
	flag.StringVar(&portFlag, "port", "6666", "port to run the server on")
	flag.StringVar(&configPath, "config", "./", "The path to the config file")
	flag.Parse() // don't forget to call Parse to actually read the flags
	
	cfg, err := utils.LoadConfig(configPath) // pass the configPath to LoadConfig
	if err != nil {
		logrus.Fatal("Error loading config: ", err)
	}

	if portFlag != "6666" {
		cfg.Server.Port = portFlag
	}

	r := chi.NewRouter()

	server := server.CustomServer(cfg,r, logrus.StandardLogger())
	
	server.InitCoreMiddleWare()
	utils.StartMetricsServer(r)
	
	
	errChan := make(chan error)
	go func (){
		err := server.Start()
		if err != nil {
			logrus.Error(err)
			errChan <- err
		}
	}()
		
		
	logrus.Fatal(<-errChan)
}
