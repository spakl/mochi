FROM golang:1.21 as builder

WORKDIR /app
COPY . .
RUN go mod tidy
RUN CGO_ENABLED=0 GOOS=linux go build -o mochi .

FROM scratch
WORKDIR /etc/mochi
COPY --from=builder /app/mochi /usr/bin/mochi
COPY --from=builder /app/mochi.yml /etc/mochi/mochi.yml
CMD [ "/usr/bin/mochi" ]
