package types

import (	
	"time"
)

type ResponseBody struct {
	Message    string      `json:"message"`
	Status     int         `json:"status"`
	APIVersion string      `json:"apiVersion"`
	Path       string      `json:"path"`
	Timestamp  time.Time   `json:"timestamp"`
	Data       interface{} `json:"data,omitempty"`
	Error      string      `json:"error,omitempty"`
}
