package utils

import (
	"encoding/json"
	"github.com/sirupsen/logrus"
	"mochi/types"
	"net/http"
	"time"
)

func MochiJSON(w http.ResponseWriter, response types.ResponseBody, status int) error {
	config, err := LoadConfig("./") // Adjust the path or make it configurable
	if err != nil {
		logrus.Fatalf("Error loading config: %v", err)
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	response.APIVersion = config.APIVersion
	response.Status = status
	response.Timestamp = time.Now().UTC()
	return json.NewEncoder(w).Encode(response)
}

type MochiMap map[string]interface{}

func MochiJSONMap(w http.ResponseWriter, mochiMap MochiMap, status int) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	return json.NewEncoder(w).Encode(mochiMap)
}
