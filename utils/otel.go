package utils

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/exporters/jaeger"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracehttp"
	"go.opentelemetry.io/otel/exporters/stdout/stdouttrace"
	"go.opentelemetry.io/otel/exporters/zipkin"

	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	"go.opentelemetry.io/otel/sdk/trace"
	oteltrace "go.opentelemetry.io/otel/trace"
)

type TracingConfig struct {
	Enabled     bool // Add this line
	Exporter    string
	Jaeger      JaegerConfig
	Zipkin      ZipkinConfig
	Otel        OTLPConfig
	ServiceName string
}

type JaegerConfig struct {
	Endpoint string
}

type ZipkinConfig struct {
	Endpoint string
}

type OTLPConfig struct {
	Endpoint string
}

var Tracer oteltrace.Tracer

// Initialize the Tracer and TracerProvider globally
func init() {
	config, err := LoadConfig("./") // Adjust the path or make it configurable
	if err != nil {
		logrus.Fatalf("Error loading config: %v", err)
	}
	tracingEnabled := viper.GetBool("tracing.enabled")
	if tracingEnabled {
		tp, err := InitTracer(config.Tracing)
		if err != nil {
			logrus.Fatalf("Error initializing tracer: %v", err)
		}

		otel.SetTracerProvider(tp)
		otel.SetTextMapPropagator(propagation.NewCompositeTextMapPropagator(propagation.TraceContext{}, propagation.Baggage{}))

		Tracer = tp.Tracer(config.Tracing.ServiceName)
	} else {
		// If tracing is disabled, use a Noop Tracer.
		Tracer = oteltrace.NewNoopTracerProvider().Tracer(config.Tracing.ServiceName)
	}
}

func OtelMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if otel.GetTracerProvider() == oteltrace.NewNoopTracerProvider() {
			next.ServeHTTP(w, r)
			return
		}

		ctx, span := Tracer.Start(r.Context(), "mochi-otel")
		defer span.End()

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func InitTracer(config TracingConfig) (tp oteltrace.TracerProvider, err error) {
	if !config.Enabled {
		logrus.Info("Tracing is disabled.")
		return nil, nil
	}
	switch config.Exporter {
	case "jaeger":
		tp, err = initJaeger(config.Jaeger, config.ServiceName)
		if err != nil {
			logrus.Error(err)
			return nil, err
		}
	case "zipkin":
		tp, err = initZipkin(config.Zipkin, config.ServiceName)
		if err != nil {
			logrus.Error(err)
			return nil, err
		}
	case "otel":
		tp, err = initOTLP(config.Otel, config.ServiceName)
		if err != nil {
			logrus.Error(err)
			return nil, err
		}
	case "stdout":
		tp, err = initStdout(config.ServiceName)
		if err != nil {
			logrus.Error(err)
			return nil, err
		}
	default:
		err = fmt.Errorf("unsupported exporter: %s", config.Exporter)
		return nil, err
	}

	return tp, nil
}

func CreateSpan(ctx context.Context, spanName string) (context.Context, oteltrace.Span) {
	context, span := Tracer.Start(ctx, spanName)
	return context, span
}

func CreateSpanWithAttr(ctx context.Context, spanName, key, value string) (context.Context, oteltrace.Span) {
	context, span := Tracer.Start(ctx, spanName, oteltrace.WithAttributes(attribute.String(key, value)))
	return context, span
}

func initJaeger(config JaegerConfig, serviceName string) (*trace.TracerProvider, error) {
	if config.Endpoint == "" {
		return nil, fmt.Errorf("jaeger endpoint is empty")
	}

	exporter, err := jaeger.New(jaeger.WithCollectorEndpoint(jaeger.WithEndpoint(config.Endpoint)))
	if err != nil {
		return nil, err
	}

	res := resource.NewWithAttributes(
		"service.name", attribute.String("service.name", serviceName),
		// Add more resource attributes as needed
	)

	tp := trace.NewTracerProvider(
		trace.WithBatcher(exporter),
		trace.WithSampler(trace.AlwaysSample()),
		trace.WithResource(res), // Corrected this line
	)

	return tp, nil
}

func initZipkin(config ZipkinConfig, serviceName string) (*trace.TracerProvider, error) {
	if config.Endpoint == "" {
		return nil, fmt.Errorf("zipkin endpoint is empty")
	}

	exporter, err := zipkin.New(config.Endpoint)
	if err != nil {
		return nil, err
	}

	res := resource.NewWithAttributes(
		"service.name", attribute.String("service.name", serviceName),
		// Add more resource attributes as needed
	)

	tp := trace.NewTracerProvider(
		trace.WithBatcher(exporter),
		trace.WithSampler(trace.AlwaysSample()),
		trace.WithResource(res),
	)

	return tp, nil
}

func initStdout(serviceName string) (*trace.TracerProvider, error) {
	exporter, err := stdouttrace.New(stdouttrace.WithPrettyPrint())
	if err != nil {
		return nil, err
	}
	res := resource.NewWithAttributes(
		"service.name", attribute.String("service.name", serviceName),
		// Add more resource attributes as needed
	)

	return trace.NewTracerProvider(
		trace.WithBatcher(exporter),
		trace.WithSampler(trace.AlwaysSample()),
		trace.WithResource(res),
	), nil
}

func initOTLP(config OTLPConfig, serviceName string) (*trace.TracerProvider, error) {
	if config.Endpoint == "" {
		return nil, fmt.Errorf("OTLP endpoint is empty")
	}

	ctx := context.Background()

	exporter, err := otlptracehttp.New(ctx,
		otlptracehttp.WithEndpoint(config.Endpoint),
		otlptracehttp.WithTimeout(time.Second*5),
		otlptracehttp.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}

	res := resource.NewWithAttributes(
		"service.name", attribute.String("service.name", serviceName),
		// Add more resource attributes as needed
	)
	// Correcting this part to ensure the right type is passed

	tp := trace.NewTracerProvider(
		trace.WithBatcher(exporter),
		trace.WithSampler(trace.AlwaysSample()),
		trace.WithResource(res),
	)

	otel.SetTracerProvider(tp)
	otel.SetTextMapPropagator(propagation.NewCompositeTextMapPropagator(propagation.TraceContext{}, propagation.Baggage{}))

	return tp, nil
}
