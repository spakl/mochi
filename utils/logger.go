package utils

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"go.opentelemetry.io/otel/trace"
)

type key string

const (
	LoggerKey key = "logger" //0
)

type ILogger interface {
	Info(args ...interface{})
	Error(args ...interface{})
	Warn(args ...interface{})
	Debug(args ...interface{})
}

func LoggerMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// config, err := LoadConfig("./") // Adjust the path or make it configurable
		// if err != nil {
		// 	logrus.Fatalf("Error loading config: %v", err)
		// }
		loggerLevel := viper.GetString("logging.level")
		loggerFormat := viper.GetString("logging.format")
		loggerHeaderLevel := r.Header.Get("LOG_LEVEL")
		if loggerHeaderLevel != "" {
			loggerLevel = loggerHeaderLevel
		}
		logg := logrus.New()
		if loggerFormat == "JSON" || loggerFormat == "json" {
			logg.SetFormatter(&logrus.JSONFormatter{})
		} else {
			logg.SetFormatter(&logrus.TextFormatter{})
		}
		logg.SetLevel(logLevel(loggerLevel))
		// Capture details from the request and host
		logger := logg.WithFields(logrus.Fields{
			"method":        r.Method,
			"url":           r.URL.String(),
			"remoteAddr":    r.RemoteAddr,
			"userAgent":     r.UserAgent(),
			"contentLength": r.ContentLength,
		})
		logger.Info()
		logger.Debug("Request Received")
		ctx := context.WithValue(r.Context(), LoggerKey, logger)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func TraceLoggerMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		// Extracting trace ID from the context
		ctx := r.Context()
		span := trace.SpanFromContext(ctx)
		traceID := span.SpanContext().TraceID().String()
		logg := logrus.New()

		loggerLevel := viper.GetString("logging.level")
		loggerFormat := viper.GetString("logging.format")
		loggerHeaderLevel := r.Header.Get("LOG_LEVEL")
		if loggerHeaderLevel != "" {
			loggerLevel = loggerHeaderLevel
		}
		
		logg.SetLevel(logLevel(loggerLevel))
		
		if loggerFormat == "JSON" || loggerFormat == "json" {
			logg.SetFormatter(&logrus.JSONFormatter{})
			} else {
				logg.SetFormatter(&logrus.TextFormatter{})
			}
		// Capture details from the request and host
			logger := logg.WithFields(logrus.Fields{
				"method":        r.Method,
				"url":           r.URL.String(),
				"remoteAddr":    r.RemoteAddr,
				"userAgent":     r.UserAgent(),
				"contentLength": r.ContentLength,
				"traceID":       traceID, // Logging the trace ID
			})
		logger.Info()
		
		logger.Debug("Request Received")
		ctx = context.WithValue(r.Context(), LoggerKey, logger)
		next.ServeHTTP(w, r.WithContext(ctx))

		elapsed := fmt.Sprint(time.Since(start).Milliseconds()) + "ms"
		logger.WithFields(logrus.Fields{
			"duration": elapsed,
		}).Info("Request processed")
	})
}

func logLevel(level string) logrus.Level {
	switch level {
	case "debug":
		return logrus.DebugLevel
	case "DEBUG":
		return logrus.DebugLevel
	case "error":
		return logrus.ErrorLevel
	case "ERROR":
		return logrus.ErrorLevel
	case "warn":
		return logrus.WarnLevel
	case "WARN":
		return logrus.WarnLevel
	case "info":
		return logrus.InfoLevel
	case "INFO":
		return logrus.InfoLevel
	default:
		logrus.Warnf("log level %s not found", level)
		return logrus.InfoLevel
	}
}
