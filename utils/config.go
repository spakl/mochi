package utils

import (
	"github.com/sirupsen/logrus"

	"github.com/spf13/viper"
)

type Config struct {
	APIVersion string
	Server     ServerConfig
	Logging    LoggingConfig
	Tracing    TracingConfig
	Metrics    MetricsConfig
}

type ServerConfig struct {
	Port         string
	StartMessage bool
}

type LoggingConfig struct {
	Level string
}

func LoadConfig(path string) (config Config, err error) {
	viper.AddConfigPath(path)
	viper.AddConfigPath("./")
	viper.AddConfigPath("/etc/mochi/")
	viper.AddConfigPath("/")
	viper.SetConfigName("mochi")
	viper.SetConfigType("yml")
	viper.SetEnvPrefix("MOCHI")

	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	if err != nil {
		logrus.Fatalf("Error reading config file, %s", err)
		return
	}

	err = viper.Unmarshal(&config)
	return
}
