package utils

import (
	"context"

	"github.com/go-chi/chi/v5"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

type MetricsConfig struct {
	Enabled      bool // Indicates if metrics should be exposed
	TraceEnabled bool
}

func StartMetricsServer(r chi.Router) {
	if viper.GetBool("metrics.traceenabled") {
		ctx := context.Background()
		_, span := CreateSpan(ctx, "metrics")
		defer span.End()
	}

	if viper.GetBool("metrics.enabled") {
		r.Handle("/metrics", promhttp.Handler())
	} else {
		logrus.Info("Metrics not enabled")
	}
}
