![src](./docs/mochi.png)
```txt
-----------------------------------------------------------------------
    _           _                            _                _          
   (_) _     _ (_)                          (_)              (_)         
   (_)(_)   (_)(_)    _  _  _       _  _  _ (_) _  _  _    _  _          
   (_) (_)_(_) (_) _ (_)(_)(_) _  _(_)(_)(_)(_)(_)(_)(_)_ (_)(_)         
   (_)   (_)   (_)(_)         (_)(_)        (_)        (_)   (_)         
   (_)         (_)(_)         (_)(_)        (_)        (_)   (_)         
   (_)         (_)(_) _  _  _ (_)(_)_  _  _ (_)        (_) _ (_) _       
   (_)         (_)   (_)(_)(_)     (_)(_)(_)(_)        (_)(_)(_)(_)      

-----------------------------------------------------------------------
```
# Mochi Starter Template 🍡

A robust, user-friendly, and efficient starter template to build powerful and scalable applications quickly and effortlessly. Mochi is designed to be flexible, easy to use, and capable of adapting to a variety of use cases.

## Features:
- **Pre-configured OTel Tracing:** With support for Jaeger, Zipkin, and OTLP out of the box.
- **Logging Middleware:** Integrated with logrus for efficient logging.
- **Flexible Configuration:** Easily tweak the settings using the config file.
- **Metrics Collection:** Prometheus metrics collection enabled by default, customizable according to your needs.

## Quick Start

### 1. Clone the Repository:
```sh
git clone https://gitlab.com/spakl/mochi.git
cd mochi
```

### 2. Build the Project:
```sh
make build
```

### 3. Run the Application:
```sh
make start
```

## Configuration

All application configurations are stored in a `mochi.yml` file. Update this file to suit your application's needs. The configurations include server settings, tracing configurations, and logging levels.

## Metrics

Prometheus metrics are enabled by default and can be accessed at `/metrics`. You can disable or configure this in the `mochi.yml` file.

## Tracing

OTel tracing is pre-configured and supports Jaeger, Zipkin, and OTLP. Customize the tracing settings in the `mochi.yml` file.

## Logger 📝

Mochi comes equipped with a versatile logger utility and middleware, ensuring that every action and transaction within your application is appropriately logged and accessible.
Core Features

- Adaptable Logging Levels: Customize the log’s verbosity by setting the log level to debug, info, warn, or error.
- Structured Logs: Gain actionable insights with structured logs, thanks to the integration of the logrus library.
- Trace Integration: With OpenTelemetry, correlate logs with traces, unraveling complex transaction paths and interactions.
- Middleware Support: Utilize the LoggerMiddleware and TraceLoggerMiddleware for automated, context-rich log entries.

### How to Use

In your main.go, the server and core middlewares, including the logger, are initialized. The logger’s configuration is loaded from the config file, with log level and format adjustments made on the fly.
Customizing Log Entries

Custom log entries can be crafted with fields capturing essential data, ensuring each log entry is rich with context, aiding in debugging and analytics.
Example

```go

// A custom log entry with fields
logger := logrus.WithFields(logrus.Fields{
	"method":        r.Method,
	"url":           r.URL.String(),
	"remoteAddr":    r.RemoteAddr,
	"userAgent":     r.UserAgent(),
	"contentLength": r.ContentLength,
})
// Logging an info entry
logger.Info("Request received")
```

## Metrics 📈

Mochi’s metrics utility is powered by Prometheus, offering detailed insights into the application’s operational metrics.

### Enable/Disable Metrics

Toggle metrics collection by adjusting the enabled attribute in the metrics section of the mochi.yml file.
Default Metrics Endpoint

Access metrics at the default /metrics endpoint or configure a custom path in the mochi.yml file.

## Docker Compose

Docker compose files for Jaeger and Zipkin are included for quick setup of tracing environments.


## Show Your Support

Give a ⭐️ if this project helped you!
