VERSION=v1.0.0

build: 
	/usr/local/go/bin/go build -o bin/mochi


start: build 
	./bin/mochi 

docker:
	sudo docker build -t spakl/mochi:${VERSION} .
	sudo docker push spakl/mochi:${VERSION}
	sudo docker tag spakl/mochi:${VERSION} spakl/mochi:latest